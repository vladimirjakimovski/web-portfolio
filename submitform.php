<?php

error_reporting();

if ($_POST) {
    $name = htmlspecialchars($_POST['name']);
    $email = htmlspecialchars($_POST['email']);
    $subject = htmlspecialchars($_POST['subject']);
    $formMessage = htmlspecialchars($_POST['message']);

    if (!empty($name) && !empty($email) && !empty($subject) && !empty($formMessage)) {

        $name = filter_var($name, FILTER_SANITIZE_STRING);
        $email = filter_var($email, FILTER_SANITIZE_EMAIL);
        $subject = filter_var($subject, FILTER_SANITIZE_STRING);
        $formMessage = filter_var($formMessage, FILTER_SANITIZE_STRING);
        if (empty($formMessage)) $formMessage = 'Нема дополнителни барања или порака';

        $to = 'vladimirjakimovski@gmail.com';
        $subject = 'Contact message - jakimovski portfolio';
        $message = '
        <!DOCTYPE html>
        <html lang="en">
        <head>
            <title>Contact Form</title>
            <style>
                td {
                    padding: 10px 20px;
                }
            </style>
        </head>
        <body>
            <div style="background-color: #fff; color: black; padding: 25px">
                <p>Пораката е од <b>' . $name . '</b></p>
                <h5>Информации за пораката</h5>
                <h4>Тема на порака: ' . $subject . '</h4>
                <h4>ПОРАКА</h4>
                <p>' . $formMessage . '</p>
            </div>
        </body>
        </html>
        ';

        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers = "Content-type:text/html;charset=UTF" . "\r\n";
        $headers .= "From: contact@jakimovski.website" . "\r\n";

        if (mail($to, $subject, $message, $headers)) {
            echo "Формата е испратена.";
        } else {
            echo "Формата не е испратена.";
        }
    } else {
        echo "Пополнете ги сите задолжителни полиња";
    }
}
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Контакт - Јакимовски</title>
    <style>
        body {
            background-color: darkgray;
            color: white;
            font-family: 'Montserrat', sans-serif;
        }

        a {
            font-size: 23px;
        }
    </style>
</head>

<body>

    <h1>Пораката е испратена.</h1>
    <!-- FIX THIS -->
    <!-- to show the reservation -->
    <div>
        <h5>Ако имате дополнителни прашања не се двоумете да ме контактирате на vladimirjakimovski@gmail.com</h5>
        <a href="index.html">Вратете се назад на страната</a>
    </div>

</body>

</html>