// start tabs

// var tabButtons = document.querySelectorAll('.tabContainer .buttonContainer button');
// var tabPanels = document.querySelectorAll('.tabContainer .tabPanel');

// function showPanel(panelIndex) {

// 	tabButtons.forEach(function (node) {
// 		node.style.backgroundColor = "";
// 	});
// 	tabButtons[panelIndex].style.backgroundColor = '#414984';

// 	tabPanels.forEach(function (node) {
// 		node.style.display = "none";
// 	});
// 	tabPanels[panelIndex].style.display = "block";

// }
// showPanel(0);
// uncomment it

// finish  tabs




// start scroll to

(function () {
	'use strict';

	var section = document.querySelectorAll(".section");
	var sections = {};
	var i = 0;

	Array.prototype.forEach.call(section, function (e) {
		sections[e.id] = e.offsetTop;
	});
})();

// finish scroll to


//   start scrollspy

document.addEventListener('DOMContentLoaded', function () {

	// grab the sections (targets) and menu_links (triggers)
	// for menu items to apply active link styles to
	const sections = document.querySelectorAll(".section");
	const menu_links = document.querySelectorAll("#nav-links a");


	// functions to add and remove the active class from links as appropriate
	const makeActive = (link) => menu_links[link].classList.add("active");


	const removeActive = (link) => menu_links[link].classList.remove("active");

	const removeAllActive = () => [...Array(sections.length).keys()].forEach((link) => removeActive(link));



	// change the active link a bit above the actual section
	// this way it will change as you're approaching the section rather
	// than waiting until the section has passed the top of the screen
	const sectionMargin = 200;

	// keep track of the currently active link
	// use this so as not to change the active link over and over
	// as the user scrolls but rather only change when it becomes
	// necessary because the user is in a new section of the page
	let currentActive = 0;

	// listen for scroll events
	window.addEventListener("scroll", () => {

		// check in reverse order so we find the last section
		// that's present - checking in non-reverse order would
		// report true for all sections up to and including
		// the section currently in view
		//
		// Data in play:
		// window.scrollY    - is the current vertical position of the window
		// sections          - is a list of the dom nodes of the sections of the page
		//                     [...sections] turns this into an array so we can
		//                     use array options like reverse() and findIndex()
		// section.offsetTop - is the vertical offset of the section from the top of the page
		// 
		// basically this lets us compare each section (by offsetTop) against the
		// viewport's current position (by window.scrollY) to figure out what section
		// the user is currently viewing
		const current = sections.length - [...sections].reverse().findIndex((section) => window.scrollY >= section.offsetTop - sectionMargin) - 1

		// only if the section has changed
		// remove active class from all menu links
		// and then apply it to the link for the current section
		if (current !== currentActive) {
			removeAllActive();
			currentActive = current;
			makeActive(current);
		}
	});
}, false);

//   finish scrollspy




// start typewriter effect

var TxtType = function (el, toRotate, period) {
	this.toRotate = toRotate;
	this.el = el;
	this.loopNum = 0;
	this.period = parseInt(period, 10) || 2000;
	this.txt = '';
	this.tick();
	this.isDeleting = false;
};

TxtType.prototype.tick = function () {
	var i = this.loopNum % this.toRotate.length;
	var fullTxt = this.toRotate[i];

	if (this.isDeleting) {
		this.txt = fullTxt.substring(0, this.txt.length - 1);
	} else {
		this.txt = fullTxt.substring(0, this.txt.length + 1);
	}

	this.el.innerHTML = '<span class="wrap">' + this.txt + '</span>';

	var that = this;
	var delta = 200 - Math.random() * 100;

	if (this.isDeleting) { delta /= 2; }

	if (!this.isDeleting && this.txt === fullTxt) {
		delta = this.period;
		this.isDeleting = true;
	} else if (this.isDeleting && this.txt === '') {
		this.isDeleting = false;
		this.loopNum++;
		delta = 500;
	}

	setTimeout(function () {
		that.tick();
	}, delta);
};

window.onload = function () {
	var elements = document.getElementsByClassName('typewrite');
	for (var i = 0; i < elements.length; i++) {
		var toRotate = elements[i].getAttribute('data-type');
		var period = elements[i].getAttribute('data-period');
		if (toRotate) {
			new TxtType(elements[i], JSON.parse(toRotate), period);
		}
	}
	// INJECT CSS
	var css = document.createElement("style");
	css.type = "text/css";
	css.innerHTML = ".typewrite > .wrap { border-right: 0.08em solid #fff}";
	document.body.appendChild(css);
};

// end typewriter effect


// start box flip

// var box = document.querySelector('.white-box');
// box.addEventListener( 'click', function() {
// 	box.classList.toggle('is-flipped');
// });

// end box flip


// rubberband start

// $(document).ready(function () {
// 	var letters = $('.border-left-white h5').text();
// 	var nHTML = '';
// 	for (var letter of letters) {
// 	  nHTML += "<span class='a'>" + letter + "</span>";
// 	}
// 	var letters = $('.border-left-white h5').text();
// 	$('h5').html(nHTML);
//   })


$(document).ready(function () {
	var letters = $('h1').text();
	var nHTML = '';
	for (var letter of letters) {
		nHTML += "<span class='a'>" + letter + "</span>";
	}
	var letters = $('h1').text();
	$('h1').html(nHTML);
});


// rubberband end


// hamburger menu start

var burgIcon = document.querySelector('.hamburger-menu');
var hambnavbar = document.querySelector('.nav-links');
var navlinks = document.querySelectorAll('.nav-links a li');

burgIcon.addEventListener("click", navSlide);

function navSlide() {
	//toggle nav
	hambnavbar.classList.toggle('nav-ul-active');
	// animate links
	navlinks.forEach((link, index) => {
		if (link.style.animation) {
			link.style.animation = '';
		} else {
			link.style.animation = `navLinkFade 0.5s ease forwards ${index / 7 + .1}s`;
		}
	});
	// burger animation
	burgIcon.classList.toggle('toggle');
}

navlinks.forEach(item => item.addEventListener("click", function () { hambnavbar.classList.remove('nav-ul-active'); burgIcon.classList.remove('toggle'); }));

// hamburger menu finish



// parallax finish it ------------------------

// $(window).scroll(function(e){
// 	parallax();
//   });
  
//   function parallax(){
// 	var scrolled = $(window).scrollTop();
// 	$('.hero').css('top',-(scrolled*0.04)+'rem');

// 	  $('.hero > .V_letter').css('top',-(scrolled*-0.005)+'rem');
// 	  $('.hero > .V_letter').css('opacity',1-(scrolled*.00175));
//   };

